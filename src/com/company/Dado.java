package com.company;

public class Dado {
    private int faces;

    public Dado(int faces){
        this.faces = faces;
    }

    public int getFaces(){
        return this.faces;
    }
}
