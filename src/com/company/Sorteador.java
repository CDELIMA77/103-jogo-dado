package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Sorteador {
    Dado dado = new Dado(6);
    ArrayList<Integer> facesSorteadas = new ArrayList<>();

    public void sortear() {
        Random random = new Random();
        int soma = 0;
        facesSorteadas.clear();

        for (int i = 0; i < 3; i++) {
            facesSorteadas.add(random.nextInt(dado.getFaces()) + 1);
            soma += facesSorteadas.get(i);
        }

        facesSorteadas.add(soma);

        Impressora.imprimir(facesSorteadas);
    }

    public void sortearDadosTresVezes(){
        for(int i =0; i<3; i++){
            sortear();
        }
    }
}
